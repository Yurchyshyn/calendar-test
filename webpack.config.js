const path = require('path');
const HTMLPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const APP_PATH = path.resolve(__dirname, 'app');
const DIST_PATH = path.resolve(__dirname, 'dist');

const isCamelCasedSCSS = /([A-Z][a-z0-9]*)+\.scss$/;

module.exports = {
    entry: './app/app',
    output: {
        path: DIST_PATH,
        filename: 'js/main.min.js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    resolveLoader: {
        moduleExtensions: ['-loader']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel'
            },
            {
                test: /\.scss$/,
                exclude: isCamelCasedSCSS,
                use: ExtractTextPlugin.extract({
                    use: [
                        'css',
                        'sass'
                    ],
                    fallback: 'style',
                    // hack to fix this issue:
                    // https://github.com/webpack-contrib/extract-text-webpack-plugin/issues/27
                    publicPath: '../'
                })
            },
            {
                test: isCamelCasedSCSS,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css',
                            options: {
                                modules: true,
                                importLoaders: 1,
                                localIdentName: '[name]__[local]__[hash:base64:5]'
                            }
                        },
                        'sass'
                    ],
                    fallback: 'style'
                })
            },
            {
                test: /\.(woff|woff2|ttf|eot|svg)$/,
                use: 'file?name=css/fonts/[name].[ext]'
            }
        ]
    },

    plugins: [
        new HTMLPlugin({
            filename: 'index.html',
            template: path.resolve(APP_PATH, 'index.html')
        }),

        new ExtractTextPlugin({
            filename: 'css/main.min.css',
            allChunks: true
        })
    ]
};
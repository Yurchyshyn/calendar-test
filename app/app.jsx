import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/app/App';

import './app.scss';

ReactDOM.render(<App />, document.getElementById('app'));
const mainElement = document.getElementById('app');
const DATE_STARTED_AT = new Date(mainElement.getAttribute('data-started-at'));

const INITIAL_EVENT_DATES_COUNT = 10;

export {
    DATE_STARTED_AT,
    INITIAL_EVENT_DATES_COUNT
};
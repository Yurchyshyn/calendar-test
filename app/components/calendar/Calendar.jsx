import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import cn from 'classnames';

import MONTHS_NAMES from '../../constants/monthsNames';
import DAYS_OF_WEEK_ABBRS from '../../constants/daysOfWeekAbbrs';

import styles from './Calendar.scss';

@CSSModules(styles, { allowMultiple: true })
class CalendarComponent extends Component {
    getDaysInMonth(month, year) {
        const lastDayOfTheMonth = new Date(year, month + 1, 0);

        return lastDayOfTheMonth.getDate();
    }

    generateCalendar1D(month, year) {
        const firstDay = this.getFirstDayOfMonth(month, year);

        const monthArray = Array.from({length: this.getDaysInMonth(month, year)}, (value, index) => index + 1);

        if (firstDay !== 0) {
            for (let i = 0; i < firstDay; i++) {
                monthArray.unshift(null);
            }
        }
        return monthArray;
    }

    getFirstDayOfMonth(month, year) {
        return new Date(year, month, 1).getDay();
    }

    create2DFrom1D(sourceArray, columnsCount) {
        const rowsCount = Math.ceil(sourceArray.length / columnsCount);

        return Array.from({ length: rowsCount }, (value, indexRow) =>
            Array.from({ length: columnsCount }, (value, indexColumn) => {
                const index = indexRow * columnsCount + indexColumn;

                return index < sourceArray.length ? sourceArray[index] : null;
            }));
    };

    render() {
        const {
            currentPeriod,
            activeDate,

            moveToNextPeriod,
            moveToPrevPeriod,
            changeActiveDate,
        } = this.props;

        const {
            month,
            year
        } = currentPeriod;

        const activeDay = activeDate.getDate();
        const activeMonth = activeDate.getMonth();
        const activeYear = activeDate.getFullYear();

        return (
            <div styleName="calendar-container">
                <div styleName="header">
                    <i
                            className="fa fa-chevron-left"
                            onClick={() => moveToPrevPeriod()}
                    />
                    <span>
                        {`${MONTHS_NAMES[month]} ${year}`}
                    </span>
                    <i
                        className="fa fa-chevron-right"
                        onClick={() => moveToNextPeriod()}
                    />
                </div>
                <div styleName="main-section">
                    <div styleName="day-names">
                        {
                            DAYS_OF_WEEK_ABBRS.map((dayAbbr, index) => {
                                return (
                                    <span
                                            styleName="name"
                                            key={index}>
                                        {dayAbbr}
                                    </span>
                                );
                            })
                        }
                    </div>
                    <div styleName="days-grid">
                        {
                            this.create2DFrom1D(this.generateCalendar1D(month, year), DAYS_OF_WEEK_ABBRS.length).
                                map((weekLine, rowIndex) => {
                                    return (
                                        <div
                                                key={rowIndex}
                                                styleName="week">
                                            {
                                                weekLine.map((day, columnIndex) => {
                                                    const dayDate = new Date(year, month, day);

                                                    return (
                                                        <span
                                                                key={`${rowIndex}:${columnIndex}`}
                                                                styleName={cn({
                                                                        day: true,
                                                                        active: day === activeDay &&
                                                                            month === activeMonth &&
                                                                            year === activeYear,
                                                                        clickable: day !== null
                                                                    })
                                                                }
                                                                onClick={day !== null ?
                                                                    () => changeActiveDate(dayDate) :
                                                                    null
                                                                }
                                                        >
                                                            {day}
                                                        </span>
                                                    );
                                                })
                                            }
                                        </div>
                                    );
                                })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default CalendarComponent;
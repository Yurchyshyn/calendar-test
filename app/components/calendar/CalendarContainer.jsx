import React, { Component } from 'react';
import { Container } from 'flux/utils';

import CalendarComponent from './Calendar';
import Store from '../../flux/store';
import Actions from '../../flux/actions';

@Container.create
class CalendarContainer extends Component {
    static getStores() {
        return [Store];
    }

    static calculateState() {
        return {
            calendar        : Store.getState(),
            moveToNextPeriod: Actions.moveToNextPeriod,
            moveToPrevPeriod: Actions.moveToPrevPeriod,
            changeActiveDate: Actions.changeActiveDate
        }
    }

    render() {
        const {
            calendar,
            
            moveToNextPeriod,
            moveToPrevPeriod,
            changeActiveDate
        } = this.state;
        
        return (
            <CalendarComponent
                currentPeriod     = {calendar.currentPeriod}
                activeDate        = {calendar.activeDate}
                moveToNextPeriod  = {moveToNextPeriod}
                moveToPrevPeriod  = {moveToPrevPeriod}
                changeActiveDate  = {changeActiveDate}
            />
        );
    }
}

export default CalendarContainer;

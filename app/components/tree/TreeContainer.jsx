import React, { Component } from 'react';
import { Container } from 'flux/utils';

import Store from '../../flux/store';
import Actions from '../../flux/actions';
import Tree from './Tree';

@Container.create
class TreeContainer extends Component {
    static getStores() {
        return [Store];
    }

    static calculateState() {
        return {
            calendar            : Store.getState(),
            
            changeActiveDate    : Actions.changeActiveDate
        }
    }

    render() {
        const {
            calendar,
            
            changeActiveDate
        } = this.state;
        
        return (
            <Tree
                activeDate          = {calendar.activeDate}
                eventsDates         = {calendar.eventsDates}
                currentPeriod       = {calendar.currentPeriod}
                changeActiveDate    = {changeActiveDate}
            />
        );
    }
}

export default TreeContainer;

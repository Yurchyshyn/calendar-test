import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import cn from 'classnames';

import MONTHS_NAMES from '../../constants/monthsNames';

import styles  from './Tree.scss';

@CSSModules(styles, { allowMultiple: true })
class Tree extends Component {
    constructor(...args) {
        super(...args);

        const monthsOpened = {};

        for (let monthName of MONTHS_NAMES) {
            monthsOpened[monthName] = false;
        }

        this.state = {
            monthsOpened
        };
    }

    get eventsDatesByMonths() {
        const {
            eventsDates
        } = this.props;

        const eventsDatesByMonths = {};

        for (let eventDate of eventsDates) {
            const month = MONTHS_NAMES[eventDate.getMonth()];
            
            eventsDatesByMonths[month] = eventsDatesByMonths[month] || [];
            eventsDatesByMonths[month].push(eventDate);
        }
        
        for (let month in eventsDatesByMonths){
            eventsDatesByMonths[month].sort(this.compareDates).sort(this.compareYears);
        }

        return eventsDatesByMonths;
    }

    compareYears(objA, objB){
        return objA.getFullYear() - objB.getFullYear();
    }

    compareDates(objA, objB){
        return objA.getDate() - objB.getDate();
    }

    toggleMonth(month) {
        const {
            monthsOpened
        } = this.state;

        this.setState({
            monthsOpened: {
                ...monthsOpened,
               [ month ]: !monthsOpened[month]
            }
        });
    }
    
    formatEventDate(eventDate) {
        return `${eventDate.getDate()} ${MONTHS_NAMES[eventDate.getMonth()]}, ${eventDate.getFullYear()}`;
    }

    componentWillReceiveProps(nextProps){
        const {
            activeDate,
            eventsDates
        } = this.props;

        const {
            monthsOpened
        } = this.state;

        if (nextProps.activeDate !== activeDate) {
            const currentMonth = MONTHS_NAMES[nextProps.currentPeriod.month];

            if (!monthsOpened[currentMonth] &&
                    eventsDates.map((date) => date.getTime()).indexOf(nextProps.activeDate.getTime()) !== -1) {
                this.setState({
                    monthsOpened: {
                        ...monthsOpened,
                        [ currentMonth ]: true
                    }
                });
            }
        }
    }

    render() {
        const {
            activeDate,
            eventsDates,

            changeActiveDate,
        } = this.props;

        const {
            monthsOpened
        } = this.state;

        const activeDay         = activeDate.getDate();
        const activeMonthName   = MONTHS_NAMES[activeDate.getMonth()];
        const activeYear        = activeDate.getFullYear();
        const eventsDatesByMonths = this.eventsDatesByMonths;

        return (
            <div styleName="tree-container">
                <div styleName="header"></div>
                <div styleName="months-list">
                    {
                        MONTHS_NAMES.map((month, index) => {
                            return (
                                <div
                                        styleName="month-item"
                                        key={index}>
                                    <div
                                            styleName={cn({
                                                'month-name': true,
                                                empty: !(month in eventsDatesByMonths),
                                                missing: eventsDates.map((date) => date.getTime()).indexOf(activeDate.getTime()) === -1 &&
                                                            month === activeMonthName
                                            })}
                                            onClick={month in eventsDatesByMonths ? () => this.toggleMonth(month) : null}>
                                        <i className={`fa fa-caret-${monthsOpened[month] ? 'down' : 'right'}`}/>
                                        <span>{month}</span>
                                    </div>
                                    
                                    <div styleName="events-container">
                                        {
                                            monthsOpened[month] &&
                                                eventsDatesByMonths[month].map((eventDate, index) => {
                                                    return (
                                                        <div
                                                                key={index}
                                                                styleName={cn({
                                                                    event: true,
                                                                    active: eventDate.getDate() === activeDay &&
                                                                              month === activeMonthName &&
                                                                              eventDate.getFullYear() === activeYear
                                                                })}
                                                                onClick={() => changeActiveDate(eventDate)}
                                                        >
                                                            <i className="fa fa-genderless"/>
                                                            <span>{this.formatEventDate(eventDate)}</span>
                                                        </div>
                                                    );
                                                })
    
                                        }
                                    </div>
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Tree;
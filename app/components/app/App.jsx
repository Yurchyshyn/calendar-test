import React, { Component } from 'react';
import CSSModules from 'react-css-modules';

import Tree from '../tree/TreeContainer';
import Calendar from '../calendar/CalendarContainer';

import styles  from './App.scss';

@CSSModules(styles)
class App extends Component {
    render() {
        return (
            <div styleName="app-container">
                <Tree />
                <Calendar />
            </div>
        );
    }
}

export default App;
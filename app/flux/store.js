import {ReduceStore} from 'flux/utils';

import ActionTypes from './actionTypes';
import Dispatcher from './dispatcher';
import initialState from './initialState';

class Store extends ReduceStore {
    constructor() {
        super(Dispatcher);
    }

    getInitialState() {
        return initialState;
    }
    
    reduce(state, action) {
        const {
            type,
            payload
        } = action;

        switch (type) {
            case ActionTypes.CHANGE_ACTIVE_DATE: {
                const currentMonth = payload.newDate.getMonth();
                const currentYear = payload.newDate.getFullYear();

                return {
                    ...state,
                    activeDate: payload.newDate,
                    currentPeriod: {
                        month: currentMonth,
                        year: currentYear
                    }
                };
            }
            case ActionTypes.MOVE_TO_NEXT_PERIOD: {
                const {
                    currentPeriod
                } = state;

                let month, year;

                if (currentPeriod.month < 11) {
                    month = currentPeriod.month + 1;
                    year = currentPeriod.year;
                } else {
                    month = 0;
                    year = currentPeriod.year + 1;
                }

                return {
                    ...state,
                    currentPeriod: {
                        month,
                        year
                    }
                };
            }
            case ActionTypes.MOVE_TO_PREV_PERIOD: {
                const {
                    currentPeriod
                } = state;
                
                let month, year;
                
                if (currentPeriod.month < 1) {
                    month = 11;
                    year = currentPeriod.year - 1;
                } else {
                    month = currentPeriod.month - 1;
                    year = currentPeriod.year;
                }

                return {
                    ...state,
                    currentPeriod: {
                        month,
                        year
                    }
                };
            }
            default: 
                return state;
        }
    }
}

export default new Store();
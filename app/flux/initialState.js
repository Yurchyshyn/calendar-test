import randomDate from '../utils/randomDate';
import {
    DATE_STARTED_AT,
    INITIAL_EVENT_DATES_COUNT
} from '../constants/config';

const fromDate = new Date('1/1/2010');
const toDate = new Date();
const eventsDates = Array.from({ length: INITIAL_EVENT_DATES_COUNT }, () => randomDate(fromDate, toDate));

export default {
    activeDate: DATE_STARTED_AT,
    eventsDates,
    currentPeriod: {
        month: DATE_STARTED_AT.getMonth(),
        year: DATE_STARTED_AT.getFullYear()
    }
};
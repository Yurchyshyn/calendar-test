import ActionTypes from './actionTypes';
import Dispatcher from './dispatcher';

const Actions = {
    changeActiveDate(newDate) {
        Dispatcher.dispatch({
            type: ActionTypes.CHANGE_ACTIVE_DATE,
            payload: {
                newDate
            }
        });
    },
    
    moveToNextPeriod() {
        Dispatcher.dispatch({
            type: ActionTypes.MOVE_TO_NEXT_PERIOD
        });
    },
    
    moveToPrevPeriod() {
        Dispatcher.dispatch({
            type: ActionTypes.MOVE_TO_PREV_PERIOD
        });
    }
};

export default Actions;
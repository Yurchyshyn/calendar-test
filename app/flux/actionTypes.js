const ActionTypes = {
    CHANGE_ACTIVE_DATE      : 'CHANGE_ACTIVE_DATE',
    MOVE_TO_NEXT_PERIOD     : 'MOVE_TO_NEXT_PERIOD',
    MOVE_TO_PREV_PERIOD     : 'MOVE_TO_PREV_PERIOD'
};

export default ActionTypes;
export default function (fromDate, toDate) {
    const diffBetweenFromTo = toDate.getTime() - fromDate.getTime();
    const date = new Date(fromDate.getTime() + Math.random() * diffBetweenFromTo);

    // we need to operate only dates, not times
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);

    return date;
}